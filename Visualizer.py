from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

def getDistArray(X, path):
    dist = [X[p-1][i+1] for i, p in enumerate(path[:-1])]
    return [sum(dist[:i]) for i in range(0,len(dist))]


def getMarkPath(marks, path):
    return [marks[i] for i in path]


def plotSequenceLine_Simple(dist, marks, path):
    # distarray = [0] + getDistArray(dist,path)
    marksPath = getMarkPath(marks, path)
    

    fig, ax = plt.subplots()
    ax.plot(range(len(path)),marksPath, '.-')

    ax.set(xlabel='Items', ylabel='Mark')

    plt.show()
    

def plotDistArray(distanceArray, labels, clusterlabels=[],marks=[]):
    if len(clusterlabels)==0:
        labels = [0]*len(X)
    d = [0]
    for i, dist in enumerate(distanceArray):
        if i != 0:
            d.append(dist + d[i])
        else:
            d.append(dist)
    plt.plot(d, [0]*len(d))
    plt.annotate(labels[0]-1 , (0, 0)) 
    plt.annotate(clusterlabels[0] , (0, 0.01), color='blue') 
    if len(marks)>0:
            plt.annotate(marks[labels[0]] , (0, 0.015), color='red')
    for i in range(1,len(d)):
        plt.annotate((labels[i]-1), (d[i], -0.005*(i%2))) 
        plt.annotate(clusterlabels[i], (d[i], 0.01), color='blue') 
        if len(marks)>0:
            plt.annotate(marks[labels[i]-1] , (d[i], 0.015), color='red')
    plt.show()    
   


def plotClusterLine(X,labels=[],marks=[]):
    if len(labels)==0:
        labels = [0]*len(X)
    d = []
    for i, r in enumerate(X[:-1]):
        if i != 0:
            d.append(r[i+1] + d[i-1])
        else:
            d.append(r[i+1])

    plt.plot(d, [0]*len(d))


    for i in range(len(X[1:])):
        plt.annotate(i+1 , (d[i], -0.005*(i%2))) 
        plt.annotate(labels[i] , (d[i], 0.01), color='blue') 
        if len(marks)>0:
            plt.annotate(marks[i] , (d[i], 0.015), color='red')
    plt.show()    
    

# def Plot(df, pca):
#     # print pca.explained_variance_
#     # print pca.components_
#     fig1 = plt.figure()
#     plt.plot(np.arange(53), pca.explained_variance_, 'ro-', linewidth=2)
#     plt.title('Scree Plot')
#     plt.xlabel('Principal Component')
#     plt.ylabel('Eigenvalue')

#     fig = plt.figure()
#     ax = fig.add_subplot(111, projection='3d')
#     ax.scatter(df['pc1'], df['pc2'], df['pc3'])

#     for i, txt in enumerate(df.index.values):
#         ax.text(df['pc1'][i],df['pc2'][i],df['pc3'][i],  '%s' % (txt), size=5, zorder=1,color='k')  
    
#     ax.set_xlabel('Pc1')
#     ax.set_ylabel('Pc2')
#     ax.set_zlabel('Pc3')
#     plt.show()
    