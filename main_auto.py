# %%
# Man whitney

control = [[1, 1, 3, 3, 1],
[3, 0, 4, 3, 4],
[3, 4, 2, 4, 3],
[1, 0, 3, 2, 1],
[2, 1, 5, 2, 3],
[1, 0, 1, 1, 2],
[3, 1, 4, 2, 3],
[2, 2, 4, 2, 2],
[2, 3, 3, 3, 3],
[2, 2, 1, 3, 2],
[1, 0, 2, 3, 3],
[4, 3, 4, 3, 3],
[4, 2, 3, 5, 4],
[2, 2, 3, 3, 3],
[1, 1, 2, 3, 2],
[2, 0, 1, 3, 2],
[2, 0, 3, 3, 4],
[2, 2, 4, 4, 3],
[1, 1, 3, 3, 4],
[1, 1, 1, 3, 2]]

test = [[3, 1, 1, 1, 4],
[5, 3, 3, 3, 4],
[5, 3, 2, 2, 4],
[2, 2, 3, 3, 1],
[3, 3, 1, 1, 2],
[2, 3, 2, 2, 1],
[1, 2, 1, 1, 3],
[4, 2, 2, 2, 3],
[4, 3, 4, 4, 5],
[4, 3, 2, 2, 2],
[4, 4, 3, 3, 5],
[3, 2, 1, 1, 1],
[2, 1, 2, 2, 0],
[4, 3, 3, 3, 3],
[4, 3, 3, 3, 3],
[4, 4, 3, 3, 2],
[3, 4, 5, 5, 5],
[4, 3, 2, 2, 2],
[2, 3, 3, 3, 3],
[3, 2, 2, 2, 2]]
#  %%
test = [[5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
[3, 3, 2, 2, 2, 2, 3, 2, 1, 2, 2, 3, 3],
[2, 1, 2, 1, 1, 0, 0, 2, 2, 0, 0, 1, 2],
[5, 5, 5, 4, 4, 3, 5, 5, 4, 5, 5, 4, 5],
[5, 5, 3, 5, 4, 5, 5, 5, 2, 4, 5, 5, 3],
[2, 1, 1, 1, 3, 1, 1, 2, 1, 1, 1, 1, 2],
[5, 5, 5, 4, 3, 3, 5, 5, 5, 5, 5, 5, 5],
[3, 4, 3, 3, 4, 3, 3, 3, 5, 5, 3, 3, 3],
[4, 4, 4, 3, 3, 3, 4, 4, 2, 4, 3, 3, 4],
[2, 2, 2, 1, 3, 1, 0, 0, 1, 0, 3, 1, 2],
[3, 3, 2, 2, 2, 2, 2, 2, 3, 2, 2, 3, 3],
[2, 3, 3, 3, 3, 2, 3, 2, 3, 0, 3, 3, 4],
[2, 3, 2, 1, 2, 2, 4, 2, 1, 2, 3, 3, 2],
[3, 3, 2, 2, 1, 2, 2, 2, 0, 2, 2, 3, 3],
[5, 5, 4, 4, 4, 4, 5, 5, 4, 5, 5, 5, 5],
[5, 5, 5, 5, 4, 4, 5, 5, 5, 5, 5, 5, 5],
[3, 4, 5, 5, 4, 4, 4, 4, 3, 5, 4, 3, 5],
[5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
[3, 3, 2, 2, 2, 2, 3, 2, 2, 2, 2, 3, 3],
[3, 2, 1, 1, 2, 1, 2, 2, 1, 1, 4, 2, 3]]

control = [[5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4],
[3, 3, 3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 2, 2, 3, 2, 4, 2],
[1, 0, 2, 3, 2, 2, 3, 1, 2, 1, 1, 2, 0, 2, 2, 1, 2, 3],
[5, 4, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 5, 5, 5, 2],
[5, 3, 5, 4, 5, 5, 4, 3, 5, 3, 4, 5, 5, 3, 3, 4, 4, 4],
[1, 0, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 0, 3, 1, 0, 2, 3],
[5, 4, 5, 4, 4, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
[3, 4, 5, 5, 4, 5, 4, 3, 4, 4, 5, 4, 3, 4, 5, 3, 5, 4],
[3, 4, 4, 3, 4, 4, 4, 3, 3, 4, 4, 4, 2, 5, 4, 3, 4, 1],
[2, 0, 2, 2, 2, 2, 2, 3, 1, 0, 1, 3, 0, 3, 2, 0, 2, 1],
[1, 2, 3, 3, 3, 3, 3, 2, 3, 3, 3, 4, 3, 4, 3, 2, 4, 2],
[2, 2, 3, 4, 2, 3, 4, 3, 3, 4, 4, 2, 2, 4, 5, 2, 4, 2],
[3, 1, 2, 2, 2, 2, 3, 2, 3, 3, 2, 1, 1, 3, 3, 0, 3, 3],
[1, 3, 3, 3, 3, 2, 3, 3, 3, 4, 3, 3, 3, 4, 3, 2, 4, 3],
[5, 5, 5, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5],
[5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
[5, 4, 5, 4, 4, 5, 5, 4, 4, 5, 5, 3, 4, 5, 5, 4, 5, 1],
[5, 5, 5, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 5, 5],
[3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 4, 3],
[2, 0, 3, 3, 2, 2, 2, 2, 2, 1, 2, 3, 1, 2, 3, 0, 3, 4]]
# %%
test = [[3, 3, 2, 2, 2, 2, 3, 2, 1, 2, 2, 3, 3],
[3, 3, 2, 2, 2, 2, 2, 2, 3, 2, 2, 3, 3],
[3, 3, 2, 2, 1, 2, 2, 2, 0, 2, 2, 3, 3],
[3, 3, 2, 2, 2, 2, 3, 2, 2, 2, 2, 3, 3]]

control = [[3, 3, 3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 2, 2, 3, 2, 4, 2],
[1, 2, 3, 3, 3, 3, 3, 2, 3, 3, 3, 4, 3, 4, 3, 2, 4, 2],
[1, 3, 3, 3, 3, 2, 3, 3, 3, 4, 3, 3, 3, 4, 3, 2, 4, 3],
[3, 3, 3, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 4, 3]]
# %%


# for c in control: c.sort()
# for c in test: c.sort()

# # %%
# wins = [0]*len(control[0])
# for i, c in enumerate(control[0]):
#     for t in test[0][i:]:

#         if i < t: wins[i]+=1
#         elif i == t: wins[i]+=0.5
# U = sum(wins)
# f = U/(len(test[0])+len(control[0]))
# print(f)
# %%
from scipy.stats import mannwhitneyu, levene, combine_pvalues, spearmanr
# %%
#Mann whitney
U = []
P = []
F = []
for i in range(len(test)):
    u, p =mannwhitneyu(control[i], test[i], False)
    U.append(u)
    P.append(round(p,4))
    F.append(round(u/(len(test[i]) *len(control[i]) ),3))
print(U,P,F, combine_pvalues(P))

# %%
# Levenes
W = []
P = []
F = []
for i in range(len(test)):
    u, p =levene(control[i], test[i])
    W.append(round(u,3))
    P.append(round(p,4))
    F.append(round(u/(len(test[i]) *len(control[i]) ),3))
print(W,P,F, combine_pvalues(P))


# %%
#Spearman Serial Control
S = []
P = []
for i in range(len(control[0])):
    rater = [a[i] for a in control]
    s, p = spearmanr(rater, range(20))
    S.append(round(s,3))
    P.append(round(p,3))
print(S,P, combine_pvalues(P), round(sum(S)/len(S),3))


# %%
#Spearman Serial Test
S = []
P = []
for i in range(len(test[0])):
    rater = [a[i] for a in test]
    s, p = spearmanr(rater, range(20))
    S.append(round(s,3))
    P.append(round(p,3))
print(S,P, combine_pvalues(P), round(sum(S)/len(S),3))



# %%
#Spearman Contrast Control
S = []
P = []
for i in range(len(control[0])):
    rater = [a[i] for a in control]
    s, p = spearmanr(rater[1:],rater[:-1])
    S.append(round(s,3))
    P.append(round(p,3))
print(S,P, combine_pvalues(P), round(sum(S)/len(S),3))



# %%
#Spearman Contrast TEst
S = []
P = []
for i in range(len(test[0])):
    rater = [a[i] for a in test]
    s, p = spearmanr(rater[1:],rater[:-1])
    S.append(round(s,3))
    P.append(round(p,3))
print(S,P, combine_pvalues(P), round(sum(S)/len(S),3))



# %%
# Levenes Inconsistency
W = []
P = []
F = []
for i in range(len(test)):
    u, p=levene(test[i], control[i])
    W.append(round(u,3))
    P.append(round(p,4))
    F.append(round(u/(len(test[i]) *len(control[i]) ),3))
print(W,P,F, combine_pvalues(P))


# %%
