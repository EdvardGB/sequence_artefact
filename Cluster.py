from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import DBSCAN
from sklearn.cluster import OPTICS, cluster_optics_dbscan
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import math
import numpy as np
import pandas as pd
import copy


def clustify(X,labels, m):
    for i, p in enumerate(X):
        for li, l in enumerate(labels):
            if l == labels[i] and l != -1: # Er de i samme cluster
                X[i][li] = X[i][li]/m
            else:
                X[i][li] = X[i][li]*m
    return X


# Calculate optimal eps
def Nnearest(X, X_copy,n):
    nearest = []
    for ri, row in enumerate(X):
        for di, d in enumerate(row):
            if di != ri and d == X_copy[ri][n+1]:
                nearest.append(d)
    return np.array(nearest)

def elbow(nearest):
    di = []
    d = 0
    p = None
    for i, n in enumerate(nearest[:-1]):
        di.append(nearest[i+1] - n)
    for i, n in enumerate(di[:-1]):
        if d < di[i+1] - n:
            d = di[i+1] - n
            p = nearest[i]
    return p    

def findOptimalEps(X):
    X_copy = copy.deepcopy(X)
    X_copy.sort()
    nearest = Nnearest(X,X_copy, 3)#math.floor(len(X)/10) +1)
    nearest.sort()
    # plt.plot(nearest)
    # plt.show()
    return elbow(nearest)




    


# def getMin(frame):
#     mi = 1000
#     a = 0
#     b = 0
#     for i, r in frame.iterrows():
#         for c in range(0,len(r)):
#             if(r[c] < mi and r[c] != 0.0):
#                 mi = r[c]
#                 a = i
#                 b = c
#     return(a, frame.keys()[b], mi)


# def Hierarchy(collections, k):
#     # Distance calculation
#     dist = []
#     for c in collections:
#         m = []
#         for h in collections:
#             m.append(Euclid(c, h)) #Euclid #WeightedEuclid  #SimpleWeightedEuclid
#         dist.append(m)


#     dist = pd.DataFrame(dist, index=k, columns=k)
#     k = [str(l) for l in k]
#     hierarchy = ""
#     scores = []
#     for l in range(0,len(dist)-1):
#         A,B, mi = getMin(dist)
#         scores.append(mi)
#         a = dist[A]
#         b = dist[B]
#         nc = []
#         nr = []
#         T = B+":"+A
#         hierarchy = T
#         k.remove(A)

#         k.remove(B)
#         k.append(T)
#         for i in range(0,len(dist)):
#             if(a[i] <= b[i] and a[i] != 0.0 and a[i] != mi and b[i] != mi):
#                 nc.append([a[i]])
#                 nr.append(a[i])
#             elif(b[i] != 0.0  and a[i] != mi and b[i] != mi):
#                 nc.append([b[i]])
#                 nr.append(b[i])
#         dist = dist.drop([A,B], axis=0)
#         dist = dist.drop([A,B], axis=1)
#         if len(nc) != 0:
#             dist = dist.to_numpy()
#             dist = np.append(dist,nc, axis=1)
#             nr.append(0.0)
#             dist = np.append(dist,[nr], axis=0)
#             dist = pd.DataFrame(dist, index=k, columns=k)
#         else:
#             break
#     return hierarchy, scores







# def KMeans(dist, k):
#     df = createDataFrame(dist, k)
#     print df.head()
#     return [], 0
