import numpy as np
import matplotlib.pyplot as plt
from collections import Counter
import copy
from Cluster import OPTICS

from statistics import stdev

# Hva er en god rekkefølge?
# - Alle karakterer gruppert
# - 

def SequencePurity(marks):
    cScore = ClusterScore(marks)
    s = 0
    for i, m in enumerate(marks[1:]):
        if abs(marks[i]-m) > 2:
            s+=1
            print(m)
    print(s)
    return s/cScore if cScore != 0 else 0

def ClusterScore(marks): #returnerer [0,1]
    print(marks)
    s = 0 
    pure = True
    for i, m in enumerate(marks[1:]):
        if m != marks[i] and pure:
            pure = False
        elif m != marks[i] and not pure and Counter(marks)[marks[i]]>1:
            s+=1
        elif m == marks[i]:
            pure = True
    return s/len(marks)/0.75 if s != 0 else 0



def VolatilitySTD(marks): #Gir feil svar pga hvis alle er forskjellige eller like, gir den 0. Ellers [0,1]
    print(marks)
    v = []
    for i, m in enumerate(marks[1:]):
        v.append(abs(marks[i]-m))
    return stdev(v)


def Volatility(marks):
    v = []
    for i, m in enumerate(marks[1:]):
        v.append(m-marks[i])
    return v


def PlotOptimalEps(dist):
    colors=['b','g','r','c','m','y','k','dimgray','brown', 'orange','sienna','tan','olive','lime','teal','navy']
    X = np.array(dist)
    X_copy = copy.deepcopy(X) #0.45 / findOptimalEps(X)
    for k in range(100,0,-10):
        fig, ax = plt.subplots()
        s=0
        labels = []
        for i in range(k,k-10,-1): 
            clust = OPTICS(min_samples=2, eps=i/100,cluster_method='dbscan').fit(X_copy)
            keys = [i for i in Counter(clust.labels_).keys()]
            values = [i for i in Counter(clust.labels_).values()]
            s = 0
            for keyi, key in enumerate(keys):
                if key not in labels:
                    ax.bar(str(i/100), values[keyi], color=colors[key % len(colors)], bottom=s, label=key)
                    labels.append(key)
                else:
                    ax.bar(str(i/100), values[keyi], color=colors[key %  len(colors)], bottom=s)
                # plt.bar(str(i/100), values[keyi], color=colors[key])
                s+= values[keyi]#values[keyi]
        ax.legend()
        ax.set_ylabel('Items')
        ax.set_xlabel('EPS')
        ax.set_title(str(k))

    plt.show()


