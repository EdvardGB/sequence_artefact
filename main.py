# %%
from Tokenizer import *
from Cluster import *
from Comparator import *
from Old_Tokenizer import Tokenize_Old
from Scanner import *
from Evaluate import * 
from Visualizer import *

import statistics

# from Feature import *
import string
import pandas as pd
import numpy as np
from numpy import random as nprandom

# import matplotlib.pyplot as plt
from tsp_solver.greedy import solve_tsp
from statistics import stdev, mean

from heldKarp import *

import copy
import random

# %%
#File handling

# files, marks = Scanner_old('test.txt')
oppgave = 'q18'
files, marks, fileorder = ScannerQ4('kode 2019/'+oppgave+'_code_and_points') #kode 2019/q4_code_and_points
imagefolder = 'bilder/'+oppgave+'/'
marks = [float(mark) for mark in marks]
numberOfFiles = len(files) 
offset = 0


assert len(files) == len(marks)
assert len(marks) == len(fileorder)

# del files[3]
# del marks[3]

# %%
colors=['b','g','r','c','m','y','k','darkorange','orangered','gold','purple','peru','pink','violet','dimgray','brown', 'orange','sienna','tan','olive','lime','teal','navy']
def JPlag():

    docs = copy.deepcopy(files[offset:numberOfFiles+offset])
    docs = [Tokenize(Scanner(doc)) for doc in docs]
    docs = StringifyDocs(docs)
    # for d in docs: random.shuffle(d)

    #Original greedy tile
    tiles = [[(GreedyStringTiling(docs[a],docs[b]), len(docs[a]), len(docs[b])) for a in range(0,len(docs))] for b in range(0,len(docs))]
    tiles_code = [[(GreedyStringTilingCode(docs[a],docs[b]), len(docs[a]), len(docs[b])) for a in range(0,len(docs))] for b in range(0,len(docs))]
    dist = [[1-TileDistance(t[0],t[1],t[2]) for t in tiles[i]] for i in range(0,len(tiles))] 
    #Tile distance er 1 hvis helt lik, derfor må den inverteres!!!

    # Improved greey tile
    # matches = [[GreedyStringTilingImproved(docs[a],docs[b]) for a in range(0,len(docs))] for b in range(0,len(docs))]
    # dist = [[1-TileDistanceImproved(match) for match in row] for row in matches]  
    return dist, tiles_code


def cluster(dist,Eps):
    X = np.array(dist)
    X_copy = copy.deepcopy(X) #0.45 / findOptimalEps(X)
    clust = OPTICS(min_samples=2, eps=Eps,cluster_method='dbscan').fit(X_copy)
    clustify(X, clust.labels_, 10)
    
    reachability = clust.reachability_[clust.ordering_]
    labels = clust.labels_[clust.ordering_]
    plt.figure(figsize=(15, 3.5))
    ax1 = plt.subplot()
    plt.rc('font', size=10) 
    space = np.arange(len(X))
    for klass, color in zip(range(0, len(colors)), colors):
        Xk = space[labels == klass]
        Rk = reachability[labels == klass]
        ax1.plot(Xk, Rk, color)
    ax1.plot(space[labels == -1], reachability[labels == -1], 'k.')
    # ax1.plot(space, np.full_like(space, 2., dtype=float), 'k-')
    # ax1.plot(space, np.full_like(space, 0.5, dtype=float), 'k-.')
    ax1.set_ylabel('Reachability distance')
    ax1.set_xlabel('Items')
    ax1.set_title('Reachability Plot')

    plt.savefig(imagefolder + 'reachability.pdf')
    plt.show()
    return X, clust


def solveTSP(X, n):
    
    # Add neutral node
    X = np.concatenate((np.array([[0.0000001]*n]).T, X), axis=1)
    X = np.concatenate( (np.array([[0.0000001]*(n+1)]), X), axis=0)

    path = solve_tsp(X)

    i = path.index(0)
    return  [i-1 for i in path[i+1:] + path[:i]]

def PlotOptimalEps3(dist):
    X = np.array(dist)
    X_copy = copy.deepcopy(X) #0.45 / findOptimalEps(X)
    # for k in range(100,0,-25):
    fig, ax = plt.subplots(figsize=(20,5))
    s=0
    labels = []
    for i in range(100,0,-1): 
        clust = OPTICS(min_samples=2, eps=i/100,cluster_method='dbscan').fit(X_copy)
        keys = [i for i in Counter(clust.labels_).keys()]
        values = [i for i in Counter(clust.labels_).values()]
        s = 0
        for keyi, key in enumerate(keys):
            if key not in labels:
                ax.bar(str(i/100), values[keyi], color=colors[key % len(colors)], bottom=s, label=key)
                labels.append(key)
            else:
                ax.bar(str(i/100), values[keyi], color=colors[key %  len(colors)], bottom=s)
            # plt.bar(str(i/100), values[keyi], color=colors[key])
            s+= values[keyi]#values[keyi]
    ax.legend()
    ax.set_ylabel('Items')
    ax.set_xlabel('EPS')
    plt.xticks(rotation=70)
    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), handletextpad=0.0001,  loc='lower left', ncol=len(labels), mode="expand", borderaxespad=0.,)
      
    # ax.set_title(str(k))
    fig.savefig(imagefolder + 'EPS.pdf')
    plt.show()


def plotClusterMarks(labels):
    clusterMarks = {}
    for l in labels: 
        if l not in clusterMarks.keys(): 
            clusterMarks[l] = []
    for i,l in enumerate(labels):
        clusterMarks[l].append(marks[i])
    mean = [sum(clusterMarks[l])/len(clusterMarks[l]) for l in clusterMarks.keys()]
    std = [stdev(clusterMarks[l]) for l in clusterMarks.keys()]
    minmark = [min(clusterMarks[l]) for l in clusterMarks.keys()]
    markrange = [max(clusterMarks[l])-min(clusterMarks[l]) for l in clusterMarks.keys()]

    fig, (a1, a0) = plt.subplots(1, 2, gridspec_kw={'width_ratios': [1, 2]}, figsize=(15,5))
    # ax.bar(clusterMarks.keys(), markrange, 0.35, yerr=std, bottom=minmark)
    # plt.errorbar(clusterMarks.keys(), mean, std, linestyle='None', marker='_')
    for i,key in enumerate(clusterMarks.keys()):
        a0.errorbar(key, mean[i], std[i], linestyle='None', marker='_', color=colors[list(clusterMarks.keys())[i]%len(colors)])
    a0.set_xticks(list(clusterMarks.keys()))
    a0.set_ylabel('Grades')
    a0.set_xlabel('Clusters')
    a0.set_title('Cluster mean & standard deviation')
    #Plotting grades

    distict_marks = []
    count_marks = []
    for mark in marks:
        if mark not in distict_marks:
            distict_marks.append(mark)
            count_marks.append(1)
        else:
            count_marks[distict_marks.index(mark)] +=1

    distict_marks, count_marks = zip(*sorted(zip(distict_marks, count_marks)))
    distict_marks = [str(i) for i in distict_marks]
    a1.bar(distict_marks, count_marks, width=0.2, align='center')
    a1.set_ylabel('Amount')
    a1.set_xlabel('Grades')
    a1.set_title('Assigned grades')
    a1.set_xticks(range(len(distict_marks)),distict_marks)

    fig.savefig(imagefolder + 'cluster_stdev.pdf')
        # set_xticks(range(1,len(path)))
        # ax.plot(i,marksPath[i], 'o', color=colors[labels[i]%len(colors)], label=labels[i])

# %%

# Create distance matrix
try:
    distFile = open(imagefolder+"dist.txt", "r")
    dist = [eval(r) for r in distFile.readlines()]
    distFile.close()
except:
    dist, tiles_code = JPlag()
    distFile = open(imagefolder+"dist.txt", "w+")
    for d in dist:
        distFile.write(str(d) + '\n')
    distFile.close()
# %%
# Plot Optimal EPS
# PlotOptimalEps2(dist)
# PlotOptimalEps3(dist)
try:
    epsFile = open(imagefolder+"eps.txt", "r")
    eps = float(epsFile.readlines()[0])
    epsFile.close()
except Exception as e:
    eps = 0.5
    epsFile = open(imagefolder+"eps.txt", "w+")
    epsFile.write(str(eps))
    epsFile.close()
 # %%
#Cluster
X, clust = cluster(dist, eps)
# %%
# plotClusterMarks(clust.labels_)
# %%
# Generate average std for clustermarks

# %%
# Solve TSP
path = solveTSP(X,numberOfFiles) 
randomPath = [nprandom.permutation(path) for i in range(1000)]
# path = held_karp(X)[1]
# path = [i+1 for i in [15,17,7,8,0,4,16,18,5,9,14,1,11,19,13,20,6,2,12,10]] 
# path = [0, 4, 16, 5, 6, 10, 13, 11, 14, 19, 1, 18, 9, 12, 2, 17, 7, 15, 8, 3]
# %%
statistics.median([1,2,3])
# %%
# Generate GST similarity clustering grade average std
# For all values of EPS
# Cluster -< Clustify -> TSP -> find clustergrades avg sum

def GSTGradesCorrelation(dist):
    std = []
    stdr = []
    clustercounter = 0
    randommarks = getMarkPath(marks,randomPath[0])
    
    clustersizeavg = []
    for eps in range(100):
        X = np.array(dist)
        clust = OPTICS(min_samples=2, eps=eps/100,cluster_method='dbscan').fit(np.array(X))
        clustify(X, clust.labels_, 10)
        # path = solveTSP(X,numberOfFiles) 
        clusterMarks = {}
        for i,l in enumerate(clust.labels_): 
            if l not in clusterMarks.keys() and l != -1: 
                clusterMarks[l] = []
            elif l != -1:
                clustercounter +=1
                clustersizeavg.append(len(clusterMarks[l]))
                clusterMarks[l].append(marks[i])
        s, sr = ([],[])
        counter = 0
        for l in clusterMarks.keys():
            s.append(stdev(clusterMarks[l]) if len(clusterMarks[l]) > 1 else 0)
            m = randommarks[counter:counter+len(clusterMarks[l])]
            try:
                assert len(clusterMarks[l]) == len(m)
            except:
                print(len(clusterMarks[l]), len(m), l, clusterMarks.keys(), counter )
            sr.append(stdev(m) if len(m)>1 else 0)
            counter += len(clusterMarks[l])
        std.append(sum(s)/len(s))
        stdr.append(sum(sr)/len(sr))
    clustersizeavg.sort()
    return sum(std)/len(std), sum(stdr)/len(stdr), clustercounter,statistics.median(clustersizeavg), sum(clustersizeavg)/len(clustersizeavg)
avgStd, avgStdr, clustcount, median, avgsize= GSTGradesCorrelation(dist)
stdFile = open(imagefolder+"stdAvgEpsClust.txt", "w+")
stdFile.write('clustered: ' +str(avgStd) +'\n')
stdFile.write('randomize: ' +str(avgStdr)+'\n')
stdFile.write('clusters: ' +str(clustcount)+'\n')
stdFile.write('clust size avg: ' +str(avgsize)+'\n')
stdFile.write('clust size med: ' +str(median))
stdFile.close()
# %%
# Path-distance
def pathDistance(path, dist):
    distance = 0
    for i,j in enumerate(path[1:]):
        distance += dist[path[i]][j]
    return distance



pathDistance(range(0,len(path)),dist)
pathFile = open(imagefolder+"pathDist.txt", "w+")
pathFile.write('optimal path-dist:' + str(pathDistance(path, dist)) +'\n')
pathFile.write('original path-dist:' + str(pathDistance(range(0,len(path)),dist))+'\n')
pathFile.write('1000 path-dist:' + str(sum([pathDistance(i,dist) for i in randomPath])/1000)+'\n')
pathFile.close()

# %%
# Volatility
def Volatility(marks):
    v = []
    for i, m in enumerate(marks[1:]):
        v.append(m-marks[i])
    return v

# vol = Volatility(marks) 
vol = Volatility(getMarkPath(marks,[i+offset-1 for i in path]))
stdevValue = stdev(vol)
stdevFile = open(imagefolder+"stdev.txt", "w+")
stdevFile.write('optimal stdev:' + str(stdevValue) +'\n')
vol = Volatility(getMarkPath(marks,[i+offset-1 for i in range(0,numberOfFiles)]))
stdevValue = stdev(vol)
stdevFile.write('original stdev:' + str(stdevValue))
stdevFile.close()


# %%
#Plot sequence line simple
def plotSequenceLine_Simple2(dist, marks, path, t):
    
    # distarray = [0] + getDistArray(dist,path)
    # path.sort()
    marksPath = getMarkPath(marks, path)
    labels = clust.labels_
    # alphabet = string.ascii_lowercase + string.ascii_uppercase

    fig, ax = plt.subplots(figsize=(15, 4.5)) 
    ax.plot(range(0,len(path)),marksPath, '-')
    legends = []
    for x, i in enumerate(path):
        if labels[i] != -1:
            if labels[i] not in legends:
                ax.plot(x,marks[i], 'o', color=colors[labels[i]%len(colors)], label=labels[i])
                legends.append(labels[i])
            else:
                ax.plot(x,marks[i], 'o', color=colors[labels[i]%len(colors)])
        else:
            ax.plot(x,marks[i], '.', color=colors[-1])
            if labels[i] not in legends:
                legends.append(labels[i])
    
    handles, labels = ax.get_legend_handles_labels()
    labels = [int(i) for i in labels]
    labels, handles = zip(*sorted(zip(labels, handles)))
    labels = [str(i) for i in labels]
    # ax.legend(handles,labels)
    ax.legend(handles=handles, labels= labels, bbox_to_anchor=(0., 1.02, 1., .102), handletextpad=0.0001,  loc='lower left', ncol=len(legends), mode="expand", borderaxespad=0.,)
    
    
    
    ax.set(xlabel='Submissions', ylabel='Grade')
    # ax.set_xticks(range(1,len(path)))
    ax.set_yticks(list(Counter(marks).keys()))
    ax.set_xticks(range(0,len(path),5))
    ax.set_title(t +' sequence', fontsize=20,  y=1.09)
    plt.xticks(rotation=90)
    # ax.yticks(np.arange(y.min(), y.max(), 0.005))
    fig.savefig(imagefolder + t +'.pdf', bbox_inches='tight')
    plt.show()

originPath = range(0,numberOfFiles)
plotSequenceLine_Simple2(dist, marks, originPath, 'Original')
plotSequenceLine_Simple2(dist, marks, path, 'Optimal')

# %%
# Contrasting effect

def Contrast(marks):
    count = 0
    for i, m in enumerate(marks[1:]):
        if abs(m-marks[i]) >=2:
            count +=1
    return count
contrastFile = open(imagefolder+"contrast.txt", "w+")
contrastFile.write('optimal contast:' + str(Contrast(getMarkPath(marks, path))) +'\n')
contrastFile.write('original contast:' + str(Contrast(getMarkPath(marks, originPath))) +'\n')
contrastFile.write('1000 path-dist:' + str(sum([Contrast(getMarkPath(marks, i)) for i in randomPath])/1000)+'\n')
contrastFile.close()
# %%
#distance graph
# import seaborn as sns
def getMin(L):
    m = L[0] if L[0] != 0 else L[1]
    for l in L:
        if l < m and (l != 0 or L.count(0)>=2):
            m = l
    return m

dist_average = [getMin(x) for x in dist]
dist_average.sort()
dist_average = dist_average
y_pos = np.arange(len(dist_average))

plt.bar(y_pos, dist_average, align='center', alpha=0.5)
plt.ylabel('Min distance')
plt.xlabel('Submission #')
plt.savefig(imagefolder + 'similarity.pdf')
plt.show()



# %%
# Find all equal answers with different grade
for r, row in enumerate(dist):
    for c, column in enumerate(row):
        if column <= 0.05 and c != r and marks[r] != marks[c]:
            print(r, c, marks[r], marks[c])
        # elif column <= 0.05 and c != r:
        #     print(r, c, marks[r], marks[c], 'like!')

# %%



# %%
fig, ax = plt.subplots()

np.random.seed(random.randint(100, 10000000))
n_points_per_cluster = 15
C1 = [5, 4] + .6 * np.random.randn(n_points_per_cluster,2)
C2 = [4, 1] + .6 * np.random.randn(n_points_per_cluster,2)
C3 = [1, 6] + .6 * np.random.randn(n_points_per_cluster,2)
C4 = [2, 3] + .6 * np.random.randn(n_points_per_cluster,2)
K = np.vstack((C1, C2, C3, C4))
x = [k[0] for k in K]
y = [k[1] for k in K]
clust = OPTICS(min_samples=3, eps=0.7,cluster_method='dbscan').fit(K)
# plt.show()
colors = ['red', 'purple', 'blue', 'brown','black', 'magenta', 'green', 'navy', 'yellow']
clustersX = [[] for i in range(len(set(clust.labels_)))]
clustersY = [[] for i in range(len(set(clust.labels_)))]
for c in set(clust.labels_):
    for i in range(len(x)):
        if clust.labels_[i] == c:
            clustersX[c].append(x[i])
            clustersY[c].append(y[i])
for i in range(len(clustersX)):
    ax.scatter(clustersX[i],clustersY[i], c=colors[i])
ax.plot(x, y, 'C3', lw=2)
plt.show()
clust.labels_

# %%
examplePoints = np.array(
 [[ 5.14766792,  4.18932082],
 [ 5.24386626,  3.39643732],
 [ 5.90836742,  5.29709729],
 [ 6.02524954,  4.32621993],
 [ 4.45720949,  4.09831767],
 [ 5.23309009,  3.82224567],
 [ 5.78570178,  3.70336555],
 [ 5.49107738,  3.37437979],
 [ 5.61036475,  2.81942383],
 [ 4.67180128,  3.92631613],
 [ 5.63158056,  3.75649845],
 [ 4.5779668,   4.2016893 ],
 [ 5.55339166,  4.12608894],
 [ 4.31580908,  5.22751119],
 [ 4.36243806,  4.40894361],
 [ 4.75053736,  0.6666265 ],
 [ 4.82754344,  0.61815485],
 [ 4.5323085,   2.24867883],
 [ 3.63255788,  2.32319391],
 [ 3.17510509,  1.06419001],
 [ 5.36488685,  0.48232718],
 [ 2.2591312,   1.78012595],
 [ 4.3844613,   1.07675841],
 [ 3.53616099,  0.52938195],
 [ 3.08409299,  2.19775873],
 [ 3.35090737,  0.26507335],
 [ 3.65295844, -0.54463489],
 [ 3.62137341,  1.76785844],
 [ 4.08070381,  0.79724693],
 [ 4.44521443,  1.46008475],
 [ 1.02855394,  6.4476517 ],
 [ 1.59652125,  6.43089726],
 [ 0.49065932,  6.35950671],
 [ 0.79369263,  6.2015525 ],
 [ 0.63550524,  5.51101576],
 [ 0.36644025,  4.89184363],
 [ 0.81355551,  5.82731605],
 [ 1.61075164,  6.27450622],
 [ 0.80434011,  6.124199  ],
 [ 0.71709379,  5.11547789],
 [ 1.35193307,  5.85242919],
 [-0.23619897,  5.56331587],
 [ 0.31000666,  5.53157992],
 [ 1.38818478,  6.0100644 ],
 [ 1.56471392,  5.89859226],
 [ 2.59813333,  3.582734  ],
 [ 2.05028819,  2.35965805],
 [ 2.39293323,  3.45925518],
 [ 2.76838895,  2.94694028],
 [ 1.55933402,  3.0583236 ],
 [ 1.00421574,  2.22903668],
 [ 2.02832038,  3.13644264],
 [ 2.19922653,  2.76541834],
 [ 2.00628306,  3.78510458],
 [ 1.94513734,  1.72754162],
 [ 0.94556567,  3.35884017],
 [ 1.86453248,  3.3404488 ],
 [ 0.78572187,  3.47582588],
 [ 0.98033825,  3.31316653],
 [ 2.7667952,  2.7487927 ]])

example_clusters = np.array([ 0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  2,  2,
       -1,  1, -1,  2,  1,  2,  2,  1,  2, -1,  1,  2,  2,  3,  3,  3,  3,
        3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  1,  1,  1,  1,  1, -1,
        1,  1,  1,  1,  1,  1,  1,  1,  1])
example_dist = [[np.linalg.norm(a-b) for a in examplePoints] for b in examplePoints]
x,y = [[p[i] for p in examplePoints] for i in [0,1]]
# %%
# Example Clustifying distance weights
example_dist = clustify(example_dist, example_clusters, 100)
# %%
# Calcualte example distances and path
example_path = [i-1 for i in solveTSP(example_dist, len(example_dist))]
exmaple_point_path = [examplePoints[i] for i in example_path]
example_cluster_path = [example_clusters[i] for i in example_path]


# %%
#Plot Example scatter plot
clustersX = [[] for i in range(len(set(example_clusters)))]
clustersY = [[] for i in range(len(set(example_clusters)))]
for c in set(example_clusters):
    for i in range(len(example_clusters)):
        if example_clusters[i] == c:
            clustersX[c].append(examplePoints[i][0])
            clustersY[c].append(examplePoints[i][1])
x,y = [[p[i] for p in exmaple_point_path] for i in [0,1]]
# %%
def reverse(arg):
    return [arg[i] for i in range(len(arg)-1,-1,-1)]

# %%
#Normal dist optimal path
n,m = 26,38
x = x[:n] + reverse(x[n:m]) + reverse(x[m:])
y = y[:n] + reverse(y[n:m]) + reverse(y[m:])
# %%
#Clustified dist optimal path
x,y = [[p[i] for p in exmaple_point_path] for i in [0,1]]
n,m = 15,38
x = reverse(x[:n]) + [x[-2]] + [x[-1]] + reverse(x[n:-2])
y = reverse(y[:n]) + [y[-2]] + [y[-1]] + reverse(y[n:-2])

# %%
shuffle = np.array(range(len(x)))
np.random.shuffle(shuffle)
x = [x[i] for i in shuffle]
y = [y[i] for i in shuffle]
# %%
#Plot Example scatter
colors = ['red', 'green', 'blue', 'brown','black', 'magenta', 'green', 'navy', 'yellow']
fig, ax = plt.subplots()
ax.plot(x, y, 'C3', lw=2)
for i in range(len(clustersX)):
    ax.scatter(clustersX[i],clustersY[i], c=colors[i])
plt.show()


# %%
#Plot sequence histogram
# originPath = range(0,numberOfFiles)
def plotSequenceHistogram(marks):
    # fig, ax = plt.subplots()
    # ax.plot(vol, '.-')

    # ax.set(xlabel='Items', ylabel='Delta')

    # # fig.savefig("test.png")
    # plt.show()
    
    # example data

    fig, ax = plt.subplots()

    # the histogram of the data
    n, bins, patches = ax.hist(marks, len(marks), density=1)
    print(n)
    # add a 'best fit' line
    ax.plot(bins,'--')
    ax.set_xlabel('Items')
    ax.set_ylabel('Marks')
    ax.set_title(r'Histogram of IQ: $\mu=100$, $\sigma=15$')

    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()
    plt.show()

plotSequenceHistogram(marks)

# %%
#Plot Heatmap
def heatmap_Mark(marks):

    fig, ax = plt.subplots()
    im = ax.imshow(marks)

    # We want to show all ticks...
    ax.set_xticks(np.arange(len(marks)))
    ax.set_yticks(np.arange(len(marks[0])))
    # ... and label them with the respective list entries
    ax.set_xticklabels(['optimal','submittet'])
    ax.set_yticklabels('Mark sequence')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(len(marks)):
        for j in range(len(marks[0])):
            text = ax.text(j, i, marks[i][j],
                        ha="center", va="center", color="w")

    ax.set_title("Harvest of local farmers (in tons/year)")
    fig.tight_layout()
    plt.show()

markpath = getMarkPath(marks, [i+offset-1 for i in path])
heatmap_Mark([markpath, marks])

# %%

# %%
# # # # # # Writing to clusterfile
c = open("clusteredFiles.txt", "w+")
print(path)
p = path[0]
for f in path:
    c.write("\n############\nPoints:"+str(marks[f-1]) + ", file:"+str(fileorder[f-1]+offset) + ", Dist:" +str(X[p-1][f-1]) + "\n")
    for line in files[f-1]:
        if line != '\n': 
            c.write(line)
    
    # for match in tiles_code[p-1][f-1][0]:
    #     c.write(str(match[:2]) + "\n" + str(match[2:])+ "\n")
    # c.write("\n\n")
    p = f
c.close()

