import math
import copy

def compareTokens(t1,t2):
    return t1.value == t2.value #Token(type,group,value)



def BagOfWords(rows, bag):
    for row in rows:
        words = {}
        for word in row:
            if word[0] in words.keys():
                words[word[0]] +=1 
            else:
                words[word[0]] = 1

        for word in words.keys():
            if word in bag.keys():
                if words[word] > bag[word]:
                    bag[word] = words[word]
            else:
                bag[word] = words[word]
    return bag

def CreateFreqTable(rows, labels):
    table = [[0 for col in range(len(labels))] for row in range(len(rows))]
    for rowi, row in enumerate(rows):
        for word in row:
            table[rowi][labels.index(word[0])] +=1
    return table

def CreateTable(rows, labels):
    table = []
    for row in rows:
        table.append([])
        for label in labels:
            match = False 
            for word in row:
                if word[0] == label:
                    table[-1].append(row.pop(row.index(word))[1] )
                    match = True
                    break
            if not match:    
                table[-1].append(0)
        if len(row) > 0:
            print(len(row))
    return table


def CreateColumnLabels(bag):
    labels = []
    tablelabels = []
    freqlabels = []
    for key in bag.keys():
        freqlabels.append(key)
        for i in range(0,bag[key]):
            labels.append(key + str(i))
            tablelabels.append(key)
    return labels, tablelabels, freqlabels


def BooleanCompare(row1, row2):
    score = 0.0
    columns = 0
    for i in range(0,len(row1)):
        if row1[i] != 0 and row2[i] != 0:
            score +=1
            columns +=1
        elif row1[i] != 0 or row2[i] != 0:
            columns +=1
    if score == 0.0:
        return score
    return score/columns


def VectorCompare(v1,v2):
    score = 0.0
    columns = 0
    for i in range(0,len(v1)):
        if v1[i] != 0 and v2[i] != 0:
            score += 1#1/ (1+ ((abs(v1[i][0] - v2[i][0]) + abs(v1[i][1] - v2[i][1]))/15) **2 )   # math.sqrt(1+ abs(v1[i][0] - v2[i][0])**2 + abs(v1[i][1] - v2[i][1])**2)
            columns +=1
        elif v1[i] != 0 or v2[i] != 0:
            columns +=1
    if score == 0.0:
        return score
    # print score/columns
    return score/columns

def FreqCompare(v1,v2):
    score = 0.0
    for i in range(0,len(v1)):
        score += (v1[i] - v2[i])**2

    if score == 0.0:
        return score
    return math.sqrt(score)


def Comparator(table):
    scoretable = []
    for row in table:
        scoretable.append([])
        for col in table:
            scoretable[-1].append(FreqCompare(row, col))
    return scoretable


def Euclid(pcomps):
    dist = []
    for row in pcomps:
        dist.append([])
        for col in pcomps:
            dist[-1].append(0)
            for i in range(0, len(col)):
                dist[-1][-1] += (row[i] - col[i])**2
            dist[-1][-1] = math.sqrt(dist[-1][-1])
    return dist

















def Compare(point, data, weights):
    for comparepoint in data:
        if comparepoint[0] == point[0]: #Two points have same token
            score = weights[point[0]]#/(1+abs(comparepoint[1][0] + comparepoint[1][1] - point[1][0] - point[1][1]))
            data.pop(data.index(comparepoint))
            return score, data
    return 0, data

def Evaluate(collections, weights):
    result = []
    for col1 in collections:
        result.append([])
        for col2 in collections:
            score = 0.0
            for key in col1.keys():
                data1 = col1[key]
                data2 = copy.deepcopy(col2[key])
                for datapoint in data1:
                    s, data2 = Compare(datapoint, data2, weights)
                    score +=s
            result[-1].append(score)
    return result
def unmark(l):
    return [(b,False) for b in l]

def overlapMatch(matches, match):
    rangeA = [n for key in matches for n in range(key[0],key[0]+ key[2])]
    rangeB = [n for key in matches for n in range(key[1],key[1]+ key[2])]
    matchRangeA = [n for n in range(match[0],match[0]+ match[2])]
    matchRangeB = [n for n in range(match[1],match[1]+ match[2])]
    for m in matchRangeA:
        if m in rangeA:
            return True
    for m in matchRangeB:
        if m in rangeB:
            return True
    return False
            

def GreedyStringTiling(listA,listB):
    A = unmark(listA)
    B = unmark(listB)
    tiles = []
    while True:
        maxmatch = 0
        matches = []
        for ai, a in enumerate(A):
            for bi, b in enumerate(B):
                j = 0
                while ai+j <= len(A)-1 and bi+j <= len(B)-1 and compareTokens(A[ai+j][0], B[bi+j][0]) and not A[ai+j][1] and not B[bi+j][1]:
                    j+=1
                if j != 0 and j == maxmatch and not overlapMatch(matches,(ai,bi,j)):
                    matches.append((ai,bi,j))
                elif j > maxmatch:
                    matches = [(ai,bi,j)]
                    maxmatch = j
        for match in matches:
            for j in range(0,maxmatch):
                A[match[0]+j] = (A[match[0]+j][0],True)
                B[match[1]+j] = (B[match[1]+j][0],True)
            tiles.append(match)
        if maxmatch == 0:
            break
    return tiles

def TileDistance(tiles, A, B):
    #sim(A, B)=2 · coverage(tiles)/(|A| + |B|)
    # coverage(tiles) = match(a,b,length)∈tiles length.
    sumLength = sum([t[2] for t in tiles])
    return 2*sumLength/(A+B) if not A + B == 0 else 0

def GreedyStringTilingCode(listA,listB):
    A = unmark(listA)
    B = unmark(listB)
    tiles = []
    while True:
        maxmatch = 0
        matches = []
        for ai, a in enumerate(A):
            for bi, b in enumerate(B):
                j = 0
                while ai+j <= len(A)-1 and bi+j <= len(B)-1 and compareTokens(A[ai+j][0], B[bi+j][0]) and not A[ai+j][1] and not B[bi+j][1]:
                    j+=1
                if j != 0 and j == maxmatch and not overlapMatch(matches,(ai,bi,j)):
                    matches.append((ai,bi,j))
                elif j > maxmatch:
                    matches = [(ai,bi,j)]
                    maxmatch = j
        for match in matches:
            for j in range(0,maxmatch):
                A[match[0]+j] = (A[match[0]+j][0],True)
                B[match[1]+j] = (B[match[1]+j][0],True)
            m = ''.join([listA[i].value for i in range(match[0],match[0]+match[2])])
            n = ''.join([listB[i].value for i in range(match[1],match[1]+match[2])])
            tiles.append((match[0],m,match[1],n))
        if maxmatch == 0:
            break
    return tiles

# siden TileDistance bare summerer opp antall like tokens, og ikke egentlig bryr seg om de
# står sammen eller ikke. kan man like gjerne droppe den tredje For loopen.
def GreedyStringTilingImproved(listA,listB):
    A = unmark(listA)
    B = unmark(listB)
    matches = 0
    for ai, a in enumerate(A):
        for bi, b in enumerate(B):
            while ai <= len(A)-1 and bi <= len(B)-1 and compareTokens(A[ai][0], B[bi][0]) and not A[ai][1] and not B[bi][1]:
                A[ai] = (a[0],True)
                B[bi] = (b[0],True)
                ai +=1
                bi +=1
                matches +=1
    return (len(A), len(B), matches)

def TileDistanceImproved(match):
    #sim(A, B)=2 · coverage(tiles)/(|A| + |B|)
    # coverage(tiles) = match(a,b,length) #tiles length.
    return 2*match[2]/(match[0]+match[1])



# def Compare(point, data, weights):
#     for comparepoint in data:
#         if comparepoint[0] == point[0]: #Two points have same token
#             score = weights[point[0]]#/(1+abs(comparepoint[1][0] + comparepoint[1][1] - point[1][0] - point[1][1]))
#             data.pop(data.index(comparepoint))
#             return score, data
#     return 0, data

# def Evaluate(collections, weights):
#     result = []
#     for col1 in collections:
#         result.append([])
#         for col2 in collections:
#             score = 0.0
#             for key in col1.keys():
#                 data1 = col1[key]
#                 data2 = copy.deepcopy(col2[key])
#                 for datapoint in data1:
#                     s, data2 = Compare(datapoint, data2, weights)
#                     score +=s
#             result[-1].append(score)
#     return result
                


# Gives scores directly on hown many instances of elements in each category 
# def Euclid(col1, col2):
#     score = 0
#     for key in col1:
#         score += (len(col1[key]) - len(col2[key]))**2
#     score = math.sqrt(score)
#     return score


# def SimpleWeightedEuclid(col1, col2):
#     weights = {
#         "Identifier": 1,
#         "Keyword": 3,
#         "Operator": 2,
#         "Literal": 2,
#         "None": 0 
#     }
#     score = 0
#     for key in col1:
#         s = (len(col1[key]) - len(col2[key]))**2
#         score += s*weights[key]
#     score = math.sqrt(score)
#     return score


# def WeightedEuclid(col1, col2):
#     weights = {
#         "Identifier": 1,
#         "Keyword": {
#             "if": 2, 
#             "for": 4, 
#             "while": 8, 
#             "return": 1, 
#             "def": 3, 
#             ".": 6
#         },
#         "Operator": {
#             "=":2, 
#             "+":2, 
#             "*":2, 
#             "-":2, 
#             "/":3, 
#             "==":3, 
#             "!=":4, 
#             ">":4, 
#             "<":4
#         },
#         "Literal": 2,
#         "None": 0 
#     }
#     score = 0
#     exclude = ["None"]
#     for key in col1:
#         s = 0
#         if (key == "Keyword" or key == "Operator"):
#             a = sum([weights[key]["."] if "." in term[0] else weights[key][term[0]] for term in col1[key]])
#             b = sum([weights[key]["."] if "." in term[0] else weights[key][term[0]] for term in col2[key]])
#             s = a-b
#         elif key not in exclude:
#             s = ((len(col1[key]) - len(col2[key]))**2)*weights[key]
#         score += s
#     score = math.sqrt(score)
#     return score