import os

def Scanner_old(file):
    f = open(file, 'r')
    lines = f.readlines()
    files = []
    marks = []
    for line in lines:
        if(line[0] == '@'):
            files.append([])
            marks.append(line[-1])
        else:
            files[-1].append(line)
    return files, marks

def ScannerQ4(fi):
    files = []
    marks = []
    fileorder = []
    for root, dirs, filenames in os.walk(fi +'/', topdown=True):
        for name in filenames:
            path = os.path.join(root, name)
            f = open(path, 'r')
            lines = f.readlines()
            fileorder.append(int(name.strip('answer')))
            for line in lines:
                if('#Points' in line):
                    files.append([])
                    marks.append(line.split()[-1])
                else:
                    files[-1].append(line)
            f.close()
    
    # Order correctly
    d = [[]]*len(files)
    m = [[]]*len(files)
    for i,f in enumerate(fileorder):
        d[int(f)] = files[i]
        m[int(f)] = marks[i]
    files = d
    marks = m
    fileorder = [int(o) for o in fileorder]
    fileorder.sort()
    return files, marks, fileorder
        