import math

class minQueue:
    def __init__(self):
        self.Q = [] #(vi,vj,wij)
    
    def pop(self):
        v = self.Q[0]
        self.Q = self.Q[1:]
        return v
        

    def insert(self,v):
        # print "\n Inserting:", v
        r = range(0,len(self.Q))
        i = self.getInsertIndex(r, v[2])
        self.Q = self.Q[:i] + [v] + self.Q[i:]

    def getInsertIndex(self,r, w):
        i = int(math.floor(len(r)/2)) 
        # print 'r = ', [self.Q[a][2] for a in r], ", w =", w , ", i =",i
        
        if len(r) == 1 and self.Q[r[0]][2] < w:
            # print 'return', r[0]+1
            return r[0]+1
        elif i == 0:
            # print "return", 0 if len(r) == 0 else r[0]
            return 0 if len(r) == 0 else r[0]
        elif self.Q[r[i]][2] > w:
            # print self.Q[r[i]][2], '>', w
            # print 'Recursive -'
            return self.getInsertIndex(r[:i], w)
        else:
            # print self.Q[r[i]][2], '<', w
            # print 'Recursive +'
            return self.getInsertIndex(r[i:], w)



def Approx(Vq,vs):
    Q = minQueue()
    Vr = Vq.columns
    for vk in Vq:
        if vk != vs: Q.insert((vs,vk, Vq[vs][vk])) 
    Et = []
    Vt = [vs]
    while len(Vt) != len(Vr):
        v = Q.pop()
        vi, vj = v[0],v[1]
        if vj in Vt: continue
        Vt = Vt + [vj]
        Et = Et + [(vi,vj)] 
        for vk in [a for a in Vr if a not in Vt]:
            if vk != vs: Q.insert((vj,vk, Vq[vj][vk]))
    T = (Vt,Et)
