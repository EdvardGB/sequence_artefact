import string 


keys = ["Identifier","Keyword","Operator","Literal","Symbol", "None"]

Literals = ["false", "true"]
Keywords = ["for", "foreach", "if", "else", "elif", "def", "return", "len", "in", "range", "while", "list", "and", "or", "not", "find", "break"]
Operators = ["=", "+", "*", "-", "/", ">", "<", "?", "!"]
LegalChars = string.ascii_lowercase + "_" 
Exclude = ["print"]
LegalSymbols = "()}{[]:,"

def Operator(line, char):
    if len(line) >= 2 and line[0] in Operators: # Double operators '==', '!=', '+=', ...
        return line[1:], char + line[0]
    return line, char

def Litteral(line, word):
    if line == "":
        return line, word 
    elif line[0] == word[0]:
        return line[1:], word + line[0]
    return Litteral(line[1:], word + line[0])

def Keyword(line, word):
    if line == "" or line[0] not in LegalChars:
        return line, word
    return Keyword(line[1:], word + line[0])

def Digit(line, word):
    if line == "" or not line[0].isdigit():
        return line, word
    return Digit(line[1:], word + line[0])


def Scanner(line, tokens):
    if line == "": #End line
        return tokens
    
    char = line[0]
    if char == "#": # Remove comments
        return tokens
    
    elif char == " ": # Remove spaces and separate 
        return Scanner(line[1:], tokens)
    
    elif char == "'" or char == '"': # Literal
        line, word = Litteral(line[1:], char)
        tokens.append((keys[3], word) )
        return Scanner(line, tokens)
    
    elif char.isdigit() or char == "-": # Digits
        line, word = Digit(line[1:], char)
        tokens.append((keys[3], word))
        return Scanner(line, tokens)

    elif char in Operators: # Operator
        line, word = Operator(line[1:], char)
        tokens.append((keys[2],word))
        return Scanner(line, tokens)

    elif char in LegalChars: # Keyword
        line, word = Keyword(line[1:], char)
        tokens.append((keys[1], word))
        return Scanner(line, tokens)


    elif char == ".": # Dotfunctions
        line, word = Keyword(line[1:], char)
        tokens.append((keys[1], word))
        return Scanner(line, tokens)
    
    elif char in LegalSymbols: # Symbol
        tokens.append((keys[4],char))
        return Scanner(line[1:], tokens)

    tokens.append((keys[5],char))
    return Scanner(line[1:], tokens)


def Evaluate(word):
    token = word[1]
    key = word[0]
    if key == keys[1] and token in Exclude:
        return token, keys[5]
    elif key == keys[1] and token in Literals:
        return token, keys[3]
    elif key == keys[1] and token not in Keywords and "." != token[0]:
        key = keys[0]
    return token, key



def Tokenize_Old(lines):
    collection = {
        "Identifier": [],
        "Keyword": [],
        "Operator": [],
        "Literal": [],
        "Symbol": [],
        "None": []
    }

    for y, line in enumerate(lines):
        line = line.lower().rstrip()
        words = Scanner(line, [])
        for x, word in enumerate(words):
            token, key = Evaluate(word)
            collection[key].append((token, (y,x)))
    return collection