import numpy as np
import pandas as pd
# from sklearn.decomposition import PCA
# from sklearn import cluster


from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage

# Features:
# lengde
# antall av hver tokens
# antall forskjellig tokens
# posisjon


def Pca(df):
    pca = PCA(len(df.index))
    pca.fit(df)
    return pca.transform(df), pca
    


# def Hierarchy(X):
    
#     linked = linkage(X, 'single')

#     labelList = range(1, len(X[0]))

#     plt.figure(figsize=(10, 7))
#     dendrogram(linked,
#                 orientation='top',
#                 labels=labelList,
#                 distance_sort='descending',
#                 show_leaf_counts=True)
#     plt.show()

# def Kmeans(X):
    
#     max_clusters = 20

#     wcss = []
#     for i in range(1, max_clusters):
#         kmeans = KMeans(n_clusters=i, init='k-means++', max_iter=100, n_init=10, random_state=0)
#         kmeans.fit(X)
#         wcss.append(kmeans.inertia_)
#     plt.plot(range(1, max_clusters), wcss)
#     plt.title('Elbow Method')
#     plt.xlabel('Number of clusters')
#     plt.ylabel('WCSS')

#     vectors = []
#     for i in range(0, len(wcss)-1):
#         vectors.append(wcss[i]-wcss[i+1])
#     s = []
#     for i in range(0, len(vectors)-1):
#         s.append(vectors[i]/vectors[i+1])

#     k = int(s.index(max(s))) + 2 if len(s) != 0 else 1
#     plt.show()
#     kmeans = KMeans(n_clusters=k, init='k-means++', max_iter=300, n_init=10, random_state=0)
#     pred_y = kmeans.fit_predict(X)
#     plt.scatter(X[:,0], X[:,1])
#     plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=300, c='red')
#     plt.show()



# # Weight based on type?, tf-idf? Create Thesaurus?
# def TFIDF(bag, N):
#     weigths = {}
#     for key in bag.keys():
#         weigths[key] = float(bag[key][0])* (float(N)/(1+ len(bag[key][1]) ))
#     return weigths

# # def PCA(df):
    