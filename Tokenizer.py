#https://docs.python.org/3/library/token.html
#https://www.programiz.com/python-programming/keyword-list


import string
    # import numpy as np
    # import pandas as pd
import copy




class Token:
    def __init__(self, typ, group, value):
        self.type = typ
        self.group = group
        self.value = value
    def __repr__(self):
        return "Token(" + self.type + "," + self.group + "," + self.value + ")"


types = ['KEYWORD', 'FUNCTION', 'SEPERATOR', 'OPERATOR', 'LITERAL', 'VARIABLE', 'UNKNOWN']

keywords = ['FLOW', 'MOD','EXC','OTH' ] #['CONTROL_FLOW', 'MODULE' ,'EXCEPTION', 'OTHER']
keyword_identifiers = {
    'for':'FLOW', 
    'while':'FLOW', 
    'break':'FLOW', 
    'continue':'FLOW', 
    'if':'FLOW', 
    'elif':'FLOW', 
    'else':'FLOW', 
    'return':'FLOW',
    'yield':'FLOW', 
    'pass':'FLOW', 
    'try':'EXC', 
    'except':'EXC', 
    'finally':'EXC', 
    'raise':'EXC',
    
     
    'from':'MOD', 
    'with':'MOD', 
    'as':'MOD', 
    'import':'MOD', 
    'del':'OTH', 
    'assert': 'OTH'
}



functions = ['LAMB', 'FUNCTION', 'CALL', 'DEF', 'GLOBAL', 'CLASS'] # ['FUNCTION', 'FUNCTION_DEF', 'CLASS']
function_identifiers = {
    'lambda':'LAMB',
    '.':'CALL',
    'def':'DEF',
    'class':'CLASS', 
    'global':'GLOBAL',
    'nonlocal': 'GLOBAL',  
} 

seperators = ['BRACKET_START','BRACKET_END', 'COLON', 'COMMA', 'SEMICOLON']
seperator_identifiers = {
    '(':'BRACKET_START', 
    ')':'BRACKET_END', 
    ':':'COLON', 
    ',':'COMMA', 
    ';':'SEMICOLON'
}


operators = ['ARI', 'REL', 'LOG', 'BIT', 'ASS', 'CON', 'MEM', 'IDE'] #['ARITHMETIC', 'RELATIONAL', 'LOGICAL', 'BITWISE', 'ASSIGNED', 'CONDITIONAL','MEMBERSHIP', 'IDENTITY']
operator_identifiers = {
    '+':'ARI', 
    '-':'ARI', 
    '*':'ARI', 
    '**':'ARI', 
    '/':'ARI', 
    '//':'ARI',
    '%':'ARI', 
    '|':'LOG', 
    '&':'LOG',
    'not':'LOG',
    '!':'LOG', 
    '>>':'BIT', 
    '<<':'BIT', 
    '~':'BIT', 
    '^':'BIT',
    'and':'BIT',
    'or':'BIT', 
    '=':'ASS', 
    '+=':'ASS', 
    '-=':'ASS', 
    '*=':'ASS', 
    '/=':'ASS', 
    '%=':'ASS', 
    '&=':'ASS', 
    '|=':'ASS', 
    '^=':'ASS', 
    '<<=':'ASS', 
    '>>=':'ASS', 
    '**=':'ASS', 
    '//=':'ASS', 
    '@=':'ASS', 
    ':=':'ASS',
    '::=':'ASS',
    '<':'REL', 
    '>':'REL', 
    '==':'REL',
    '===':'REL', 
    '!=':'REL', 
    '<=':'REL', 
    '>=':'REL', 
    'in':'MEM',
    'is':'IDE',
}
literals = ['BOOLEAN','LIST_START', 'LIST_END' 'COLLECTION_START', 'COLLECTION_END', 'NONE']
literal_identifiers = {
    '[':'LIST_START',
    ']':'LIST_END',
    '{':'COLLECTION_START',
    '}':'COLLECTION_END', 
    'false': 'BOOLEAN', 
    'true': 'BOOLEAN', 
    'none': 'NONE'
}

variables = ['NUMBER', 'STRING', 'VARIABLE']

stringCharacters = string.ascii_lowercase + '_'
def isString(s):
    return True in [c in stringCharacters for c in s]

def isInt(s):
    try: int(s)
    except ValueError: return False
    else: return True

def getVariableToken(pretoken, token, posttoken): # token.type: 'UNKNOWN'
    if isInt(token.value):
        return Token(types[5], variables[0], token.value), False # Int
    
    elif pretoken is not None and pretoken.group == 'DEF': 
        return Token(types[1], functions[1], token.value), False # def Function
    
    elif  posttoken is not None and posttoken.group == 'BRACKET_START':
        return Token(types[1], functions[2], token.value), True # function call
    
    elif posttoken is not None and posttoken.type == types[3]:
        return Token(types[5], variables[2], token.value), True # variable assign
    
    elif isString(token.value) or '"' in token.value or "'" in token.value: 
        return Token(types[5], variables[1], token.value), False # string
    return token, False
    


#['KEYWORD', 'FUNCTION', 'SEPERATOR', 'OPERATOR', 'LITERAL', 'VARIABLE', 'UNKNOWN']

def getToken(term):
    if term in keyword_identifiers.keys(): #Keyword
        return Token(types[0], keyword_identifiers[term], term)

    if term in function_identifiers.keys(): #Function
        return Token(types[1], function_identifiers[term], term)

    if term in seperator_identifiers.keys(): #Seperator
        return Token(types[2], seperator_identifiers[term], term)

    if term in operator_identifiers.keys(): #Operator
        return Token(types[3], operator_identifiers[term], term)
    
    if term in literal_identifiers.keys(): #Literal
        return Token(types[4], literal_identifiers[term], term)
    return Token(types[6], 'UNKNOWN', term) #Unknown


def Tokenize(doc):
    repeats = {}
    #Iterate to set types
    for row in doc:
        for i, term in enumerate(row):
            row[i] = getToken(term)
    #Iterate to set variables
    for row in doc:
        for i, token in enumerate(row):
            if token.type == types[6]: # If token is unknown
                if token.value in repeats.keys(): # if token is repeatable
                    repeatToken = repeats[token.value]
                    row[i] = Token(repeatToken.type,repeatToken.type,token.value)
                else: 
                    posttoken, pretoken = None, None
                    if i != len(row)-1:
                        posttoken = row[i+1]
                    if i != 0:
                        pretoken = row[i-1]
                    token, repeat = getVariableToken(pretoken, token, posttoken)
                    row[i] = token
                    if repeat: #Repeat token pattern for all future matches
                        repeats[token.value] = token 
    for row in doc:
        for d in row:
            if d.type == types[6]:
                print(token)           
    return doc
            
# print(Tokenize([['def','hello','1', 'hello', '(', '=']]))
# k = list(operator_identifiers.keys())
# k.reverse()
# def splitOperator(word):
#     for operator in k:
#         print(operator)
#         if operator in word:
#             i = word.index(operator)
#             words = splitOperator(word[:i]) + [operator] + splitOperator(ord[i+1:])
#     return word

def isPartOfOperator(word, i):
    keys = [key for key in list(operator_identifiers.keys()) if len(key) >= i]
    # print(word, i)
    for key in keys:
        if word == key[:i]:
            return True
    return False

def setOperator(words):
    newWords = []
    operator = ''
    for word in words:
        if len(word) == 1:
            if isPartOfOperator(operator + word, len(operator + word)) or (isPartOfOperator(word, len(word)) and operator == ''):
                operator += word
            elif len(operator) > 0:
                newWords.append(operator)
                operator = ''
                newWords.append(word)
            else:
                newWords.append(word)
        elif len(operator) > 0:
            newWords.append(operator)
            operator = ''
            newWords.append(word)
        else:
            newWords.append(word)
    if len(operator) > 0: 
        newWords.append(operator)
    return newWords 
        
        

def splitWord(word):
    splitters = [list(seperator_identifiers.keys()) + list(literal_identifiers.keys()) + ['.'] + list(operator_identifiers.keys())][0]
    operator = ''
    words = [word]
    isString = False
    for k, char in enumerate(word):
        if char in ['"', "'"] or isString:
            if char in ['"', "'"] and isString:
                words = [word[:k], char, word[k+1:]] 
                words = words[:-1] + splitWord(words[-1])
                isString = False
            else:
                isString = True
            continue
        # Splitt på operator, skilletegn og tall
        elif char in splitters:
            i = word.index(char)
            words = [word[:i], char, word[i+1:]] 
            words = words[:-1] + splitWord(words[-1])
            # print(words)
            return words
    return words


def splitLine(line):
    line = line.lower().rstrip().split(' ') # Lowercase and removing \n
    line = [s for s in line if s != ""] # Remove empty
    newline = copy.deepcopy(line)
    # print(line)
    for word in line:
        if '#' in word:
            # Remove comments
            # print('#######################',newline)
            return newline[:newline.index(word)]
        # print(line[:i]+ ['|||'] +splitWord(word)+ ['|||'] + line[i+1:])
        # print(word, newline)
        i = newline.index(word)
        newline = newline[:i]+ splitWord(word) + newline[i+1:] # Split row into terms
        # print(word, newline)
        newline = [s for s in newline if s != ""] # Remove empty
    newline = setOperator(newline) # Add correct operators
        # print(word, newline)
    return newline

def Scanner(doc):
    for r, line in enumerate(doc):
        doc[r] = splitLine(line)
        doc[r] = [l for l in doc[r] if len(l) != 0] 
    doc = [s for s in doc if s != []]
    # [print(line) for line in doc]
    return doc



def StringifyDocs(docs):
    newdocs = []
    for doc in docs:
        newdocs.append([])
        for row in doc:
            newdocs[-1] = newdocs[-1] + row
    return newdocs





