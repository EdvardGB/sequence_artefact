import numpy as np
import copy

# the edit distance d(a, b) is the minimum-weight series of edit operations that transforms a into b.

def Levenstein(s1,s2):
    dist = np.zeros((len(s1),len(s2)+1))
    dist = np.concatenate(([range(0,len(s2)+1)], dist), axis=0).tolist()
    for i in range(0,len(s1)+1):
        dist[i][0] = i 
    for i in range(1,len(s1)+1):
        for j in range(1,len(s2)+1):
            if s1[i-1] == s2[j-1]:
                dist[i][j] = dist[i-1][j-1]
            else:
                dist[i][j] = min(dist[i][j-1], dist[i-1][j], dist[i-1][j-1]) +1
    return dist[-1][-1]

def appendZero(r,z):
    for i in range(0,z):
        r.append([""])
    return r

def ReorderIfEqualRows(r1,r2):
    reorder = []
    if len(r2) < len(r1):
        r2 = appendZero(r2, len(r1) - len(r2))
    elif len(r2) > len(r1):
        r1 = appendZero(r1, len(r2) - len(r1))
    for i in range(0,len(r1)):
        for j in range(0,len(r2)):
            if r1[i] == r2[j]:
                reorder.append( (i, r2.pop(j)) )
                j -=1
                break
    for r in reorder:
        r2 = r2[:r[0]] + [r[1]] + r2[r[0]:]
    return r1, r2


def Distance(m1,m2):
    m1, m2 = ReorderIfEqualRows(copy.deepcopy(m1),copy.deepcopy(m2))
    dist = 0
    for i in range(0,len(m1)):
        dist += Levenstein(m1[i],m2[i])
    return dist

def StripCoord(row):
    rows = [[]]
    i = 0
    for word in row:
        if word[1][0] > i:
            rows.append([word[0]])
            i = word[1][0]
        else:
            rows[-1].append(word[0])
    return rows

def EditDistance(rows):
    distanceMatrix = []
    for i, row in enumerate(rows):
        rows[i] = StripCoord(row)
    for n, m1 in enumerate(rows):
        distanceMatrix.append([])
        for m, m2 in enumerate(rows):
            p = False
            if (n,m) == (0,2) or (n,m) == (2,0):
                p = True
            if n == m:
                distanceMatrix[-1].append(0)
            else:
                distanceMatrix[-1].append(Distance(m1,m2))
    return distanceMatrix